from flask import Flask, request
import pandas as pd
import numpy as np
import joblib

app = Flask(__name__)

@app.route("/predict", methods=["GET"])
def predict():

    name = request.args.get("name")
    vectorizer = joblib.load("vectorizer.joblib")
    name_vector = vectorizer.transform([name])
    model = joblib.load("trained_model.joblib")
    prediction = model.predict(name_vector)
    print(prediction[0],"******************")
    prediction=f"{prediction[0]}"
    return f"le sexe du prénom {name} est: {prediction}"

if __name__ == "__main__":
    app.run(port=5001)
